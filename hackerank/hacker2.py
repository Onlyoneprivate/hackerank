def alternatingCharacters(s):
    # Write your code here
    count = 0
    for index in range(len(s)-1):
        if s[index] == s[index+1]:
            count += 1
        else:
            continue
    return count
