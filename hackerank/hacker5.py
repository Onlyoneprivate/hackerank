def gridChallenge(grid):
    # Write your code here
    grid =[list(row) for row in grid]
    
    r = len(grid)
    c = len(grid[0])

    for i in range(len(grid)):
        grid[i] = sorted(grid[i])

    for column in range(c):
        for row in range(r-1):
            if not grid[row][column] <= grid[row+1][column]:
                return 'NO'
    return 'YES'
