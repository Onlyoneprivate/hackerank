import unittest
from hackerank.hacker5 import gridChallenge

class GridChallengeTest(unittest.TestCase):
    
    def test_gridchallenge_1(self):
        grid_list = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        grid_test = gridChallenge(grid_list)
        self.assertEqual('YES',grid_test)
    
    def test_gridchallenge_2(self):
        grid_list = ['abc', 'lmp', 'qrt']
        grid_test = gridChallenge(grid_list)
        self.assertEqual('YES',grid_test)
    
    def test_gridchallenge_3(self):
        grid_list = ['mpxz', 'abcd', 'wlmf']
        grid_test = gridChallenge(grid_list)
        self.assertEqual('NO',grid_test)
    
    def test_gridchallenge_4(self):
        grid_list = ['abc', 'hjk', 'mpq', 'rtv']
        grid_test = gridChallenge(grid_list)
        self.assertEqual('YES',grid_test)
    
if __name__=="__main__":
    unittest.main()
