import unittest
from hackerank.hacker4 import alternate

class TwoCharactersTest(unittest.TestCase):
    
    def test_two_char_alternating_1(self):
        alphabet = 'beabeefeab'
        two_char_alternating = alternate(alphabet)
        self.assertEqual(5,two_char_alternating)
    
    def test_two_char_alternating_2(self):
        alphabet = 'asdcbsdcagfsdbgdfanfghbsfdab'
        two_char_alternating = alternate(alphabet)
        self.assertEqual(8,two_char_alternating)
    
    def test_two_char_alternating_3(self):
        alphabet = 'asvkugfiugsalddlasguifgukvsa'
        two_char_alternating = alternate(alphabet)
        self.assertEqual(0,two_char_alternating)
    
    
if __name__=="__main__":
    unittest.main()
