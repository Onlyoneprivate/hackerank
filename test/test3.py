import unittest
from hackerank.hacker3 import caesarCipher

class CaesarCipherTest(unittest.TestCase):
    
    def test_caesar_shift_2(self):
        alphabet = 'middle-Outz'
        shift = 2
        caesarshift = caesarCipher(alphabet,shift)
        self.assertEqual('okffng-Qwvb',caesarshift)
    
    def test_caesar_shift_5(self):
        alphabet = 'Always-Look-on-the-Bright-Side-of-Life'
        shift = 5
        caesarshift = caesarCipher(alphabet,shift)
        self.assertEqual('Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj',caesarshift)
    
if __name__=="__main__":
    unittest.main()
